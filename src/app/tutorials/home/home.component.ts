import { Component, OnInit } from '@angular/core';
import { Node } from '../tree/tree.component';
import { DataHeader } from '../header/header.component';
import { DataContent } from '../content/content.component';
import { YoutubePlayerService } from '../video/youtube-player.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    nodes: Node[] = [
        {
            id: 'intro',
            name: 'Introducción',
            children: [
                { id: 'presentacion', name: 'Presentación' },
                { id: 'vision', name: 'Visión General' }
            ]
        },
        {
            id: 'mod1',
            name: 'Módulo de configuración',
            children: [
                { id: 'dep', name: 'Conf. de Dependencias' },
                { id: 'car', name: 'Conf. de Cargos' },
                { id: 'fun', name: 'Conf. de Funcionarios' }
            ]
        }
    ];

    dataHeader: DataHeader = {
        title: 'Introducción',
        description: 'Se hace la presentación de la plataforma en forma general, luego se van desglosando los módulos.'
    };

    DataContent: DataContent[] = [
        {
            title: 'Introducción',
            content: 'Esta es la introducción del video que se está presentando, '
                + 'se puede dar algo de contexto para la visualización del contenido.'
        },
        {
            title: 'Objetivos',
            content: 'obj.Bla Bla Bla'
        },
        {
            title: 'Conclusión',
            content: 'conc.Bla Bla Bla'
        }
    ];

    disabledMenu = false;

    constructor(
        protected youtubePlayer: YoutubePlayerService,
    ) {
    }

    ngOnInit() { }

    onSelected(event) {
        console.log(event);
    }

    test() {
        this.youtubePlayer.playVideo('5wtnKulcquA');
    }

}
