import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    @Input() data: DataHeader = null;

    constructor() { }

    ngOnInit() {
    }

}

export interface DataHeader {
    title: string;
    description: string;
}
