import { Component, AfterContentInit, OnInit } from '@angular/core';
import { YoutubePlayerService } from './youtube-player.service';

@Component({
    selector: 'app-video',
    templateUrl: './video.component.html',
    styleUrls: ['./video.component.scss']
})
export class VideoComponent implements AfterContentInit, OnInit {

    constructor(
        protected youtubePlayer: YoutubePlayerService,
    ) {
    }

    ngOnInit(): void {}

    ngAfterContentInit() {
        const doc = window.document;
        const playerApi = doc.createElement('script');
        playerApi.type = 'text/javascript';
        playerApi.src = 'https://www.youtube.com/iframe_api';
        doc.body.appendChild(playerApi);
        this.youtubePlayer.createPlayer();
    }

}
