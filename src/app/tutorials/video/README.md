# Youtube Video Player

This component is inspired on an amazing video library project:  [https://github.com/SamirHodzic/ngx-youtube-player](https://github.com/SamirHodzic/ngx-youtube-player)

This component uses Youtube Iframe mechanism to work.
This component can used without restrictions.
This componen was built as part of VideoTutorials project.