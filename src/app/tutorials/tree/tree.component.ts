import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
    selector: 'app-tree',
    templateUrl: './tree.component.html',
    styleUrls: ['./tree.component.scss']
})
export class TreeComponent implements OnInit {

    @Output() selected: EventEmitter<any> = new EventEmitter();

    @Input() nodes: Node[] = [];

    options = {};

    constructor() { }

    ngOnInit() {
    }

    onEvent(event) {
        this.selected.emit(event.node.data);
    }

}

export interface Node {
    id: string;
    name: string;
    children?: Node[];
}
