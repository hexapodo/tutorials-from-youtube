import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { TreeModule } from 'angular-tree-component';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';

import { HomeComponent } from './home/home.component';
import { TreeComponent } from './tree/tree.component';
import { HeaderComponent } from './header/header.component';
import { VideoComponent } from './video/video.component';
import { ContentComponent } from './content/content.component';
import { ProgressComponent } from './progress/progress.component';

@NgModule({
  declarations: [
    HomeComponent,
    TreeComponent,
    HeaderComponent,
    VideoComponent,
    ContentComponent,
    ProgressComponent,
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatCardModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatTabsModule,
    TreeModule.forRoot(),
  ],
  exports: [
    HomeComponent
  ]
})
export class TutorialsModule { }
